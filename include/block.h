#ifndef BLOCK_H
#define BLOCK_H 

#include <string>
#include<iostream>
#include <vector>
#include "sha.h"
using namespace std;


//template <class T=int>
class CBlock
    {
    public:
    CBlock(string p_hash=""){
       nonce=0;
       data="0";
       previous_hash=p_hash;
       //update_hash();
    }
    

    string stringify()const{
        return data + std::to_string(nonce) + previous_hash;
    }

    bool is_valid()const {
        return this->get_hash() == this->hash;
    }

    unsigned int nonce;
    string data;
    string hash;
    string previous_hash; 
    
    void update_hash(){
        hash=get_hash();
        return;
    }

    string get_hash()const{
        return sha256(this->stringify()); 
    }

    void mine(){
        update_hash();
        while (!(hash.substr(0,2) == "00")){
            nonce++;
            update_hash();
        } 
    }

    private:
    };

class CBlockChain
    {
    public:
    CBlockChain(){
        auto genesis=CBlock();
        genesis.mine();
        blocks.push_back(genesis);
        }

    void add_block(CBlock block){
        block.previous_hash=blocks.back().hash;
        block.mine();
        blocks.push_back(block);
        }

    vector< CBlock > blocks;
    void print(){
        string str="";
        for(int i=0; i<blocks.size(); i++){
             
            cout<< "Block #"<< i<<endl;
            cout<< "    Valid: "<< blocks.at(i).is_valid()<<endl;
            cout<< "    Nonce: "<< blocks.at(i).nonce<<endl;
            cout<< "    Previous hash: "<< blocks.at(i).previous_hash<<endl;
            cout<< "    hash: "<< blocks.at(i).hash<<endl;
        } 
    }
    private:
    };

#endif /* BLOCK_H */
